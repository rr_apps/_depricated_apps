
require "fileutils"
require_relative './support/helper.rb'
require 'csv'

module PBTEST

  # set the variables
  time          = Time.new # generate a time stamp
  date_stamp    = time.strftime("%Y_%m_%d-%H_%M") # format the timestamp

  pwd           = File.dirname(__FILE__)                      # get the directory this file is in
  profiles_path = File.join(pwd, './../../01_Profiles')       # the profiles dir
  output_path   = File.join(pwd, './../../03_Output')         # the output dir
  music_path    = File.join(pwd, './../../02_Music')          # the music files
  cmd           = File.join(pwd, '/support/natnumslogic.sh')  # the logic for creating the ads
  profiles      = File.join(pwd, './../../profiles.csv')      # csv with the gaps settings

  # read the profiles csv file to get the gaps per voice
    profile = {}
    CSV.foreach(profiles, :headers => true, :header_converters => :symbol, :converters => :all) do |row|
      profile[row.fields[0]] = Hash[row.headers[1..-1].zip(row.fields[1..-1])]
    end

  # Get the availble voices
  vo_paths     = []                                         # define an array
  vo_profiles  = Dir.entries(profiles_path)                 # get the available voices in the die
  array_wash(vo_profiles)                                   # remove any unwanted stuff

  ## Display the details for the advert to be made.
  puts `clear`                                              # clear the terminal window
  puts "Price & Body Re-Record Test Application".center(80) # display stuff
  puts "_" * 80                                             # display stuff
  puts "\n\n"                                               # new line dispaly stuff

  # Main loop iterates through the available vo's and creates an advert for each pb using random
  # val_a's, val_b's and music.
  vo_profiles.each do |i|                                   # iterate through the available voices

    # Get the current vo's gap/pad timings
    pad_1         = profile[i][:gap1]                       # set the first pad value
    pad_2         = profile[i][:gap2]                       # set the second pad value
    pad_3         = profile[i][:gap3]                       # set the third pad value

    # Get the current vo's name & language
    the_vo, lang  = vo_selector(i)                          # set the vo name & vo language

    # The paths to the files
    vo_path       = File.join(profiles_path, i)             # set path to current vo profile
    pb_path       = File.join(vo_path, 'pb')                # set path to current vo pb's
    vala_path     = File.join(vo_path, 'val_a')             # set path to current vo val_a's
    valb_path     = File.join(vo_path, 'val_b')             # set path to current vo val_b's

    # the files
    the_pbs       = Dir.entries(File.expand_path(pb_path))      # get list of available pb's
    the_valas     = Dir.entries(File.expand_path(vala_path))    # get list of available val_a's
    the_valbs     = Dir.entries(File.expand_path(valb_path))    # get list of available val_b's
    the_music     = Dir.entries(File.expand_path(music_path))   # get list of available music

    # Remove the unwanted "./", "../", ".DS_Store" from the arrays
    array_wash(the_pbs)                                     # remove from pb array
    array_wash(the_valas)                                   # remove from val_a array
    array_wash(the_valbs)                                   # remove from val_b array
    array_wash(the_music)                                   # remove from music array

    # Sub loop that iterates through the available pb's and creates an advert for each one.
    the_pbs.each do |pb|                                    # iterate over each pb

      # Random selection of price and music for the advert
      random_vala   = the_valas[rand(the_valas.length)]     # get a random val_a
      random_valb   = the_valbs[rand(the_valbs.length)]     # get a random val_b
      random_music  = the_music[rand(the_music.length)]     # get a random music piece

      # Set the name of the advert using the pb number and appending sp
      sp = "sp" + pb.sub(/^pb/, '')                         # set advert name

       # Get the relative file paths
       ad_pb    = File.join(File.expand_path(pb_path), pb)                  # pb path
       ad_vala  = File.join(vala_path, random_vala)                         # val_a path
       ad_valb  = File.join(valb_path, random_valb)                         # val_b path
       ad_music = File.join(music_path, random_music)                       # music path
       ad_final = File.join(File.expand_path(output_path), i, date_stamp)   # output path
       ad_sp    = File.join(ad_final, sp)                                   # advert path

       # Create the output diretory if not exists
       if !Dir.exists?(ad_final) ; FileUtils.mkdir_p(ad_final) ; end        # create out dir

        # Concatenate the parts that make up the advert - see comment below.
        ad_cmd = "#{File.expand_path(cmd)} #{File.expand_path(ad_pb)} #{File.expand_path(ad_vala)} #{File.expand_path(ad_valb)} #{File.expand_path(ad_music)} #{File.expand_path(ad_sp)} #{pad_1} #{pad_2} #{pad_3}"

        # Display the advert details
        line =  "| Creating SP: #{sp[0..-5]}".ljust(10)     # ad name withouth ext.
        line << " >> Using:".ljust(10)                      # text
        line << " #{the_vo}  #{lang}"                       # vo name & lang
        line << "\n"                                        # newline
        line << "| PB: #{pb[0..-5]}".ljust(13)              # current pb
        line << "| Pad 1: #{pad_1}".ljust(13)               # first pad
        line << "| Val_a: #{random_vala[0..-5]}".ljust(13)  # the val_a
        line << "| Pad 2: #{pad_2}".ljust(13)               # the second pad
        line << "| Val_b: #{random_valb[0..-5]}".ljust(13)  # the val_b
        line << "| Pad 3: #{pad_3}".ljust(13)               # the third pad
        line << "\n" + "-" * 80 + "\n"                      # seperator line

        # Display the infor of the SP to be created.
        print line                                          # output to screen

        # Call the 'Natnumslogic.sh' script with all the files, paths & settings
        `#{ad_cmd}`
        end
     end
end

PBTEST
