## Helpers

module Helpers

  # Remove unwanted characters & words
  def array_wash(to_clean)
    to_remove = [".", "..", ".DS_Store"]
      to_remove.each do |i|
        to_clean.delete_if { |x| x == i}
      end
  end

  # A dsplay method - not used currently
  def vo_options(the_data)
    line = "\n" + "-" * 80 + "\n"
    puts line
    puts "| The Data: #{the_data}\n| The Array: #{the_array}"
    puts line
  end

  # VO selector & language parser.
  def vo_selector(voice)
    if voice.downcase.include?("eng") ; lang = "Eng" ; end
    if voice.downcase.include?("afr") ; lang = "Afr" ; end
    the_vo = voice[0..-5]
    voice = the_vo.capitalize
    return voice, lang
  end

  def echo(disp)
   puts "\n" + "-" * 80 + "\n\n"
   puts disp.center(80)
   puts "\n" + "-" * 80 + "\n\n"
 end

 # type = pb, val_a, val_b or music / true = random
 def get_path(vo_path, type="", rnd=false)

   the_path       = File.join(vo_path, type)           # set path to the dir
   the_path       = File.expand_path(the_path)             # expand the path to dir
   the_files       = Dir.entries(the_path)              # get list of available files
   array_wash(the_files)                                # purge the file list

   if rnd
     rnd_file   = the_files[rand(the_files.length)]     # get a random val_a
     the_file   = File.join(the_path, rnd_file)
     return the_file
   else
      return the_path, the_files
   end
 end
end

include Helpers



