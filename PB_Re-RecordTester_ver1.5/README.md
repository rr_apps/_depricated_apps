# PB Tester #

App to aid in the testing of re-recorded PB's for the Natural Numbers Project.
Will also be used to fine tune the per voice profile gaps, the logic to implemented at the stores and the the per voice mastering profiles.

### What is this repository for? ###

* The app (source) for the PB Tester
* Version 1.06

### How do I get set up & use this? ###


* Pull this to ~/projects/rr_apps/pb_tester
+ Dependencies
    * SoX 
    * LAME
+ How to run tests
    * Place the VO profiles into the "01_Profiles" directory and run the "pb_tester" executable.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* dteren@gmail.com
* ZaPOP IT


