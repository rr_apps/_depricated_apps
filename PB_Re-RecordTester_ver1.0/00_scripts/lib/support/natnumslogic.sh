#!/bin/sh

# Usage:
# ./natnumslogic.sh /Users/davidteren/Dropbox/03_Developement/02_Projects/1_RadioRetail/_Complete/PB_Tester/01_Profiles/jordan_eng/pb/pb000258.mp3 /Users/davidteren/Dropbox/03_Developement/02_Projects/1_RadioRetail/_Complete/PB_Tester/01_Profiles/jordan_eng/val_a/009.mp3 /Users/davidteren/Dropbox/03_Developement/02_Projects/1_RadioRetail/_Complete/PB_Tester/01_Profiles/jordan_eng/val_b/099.mp3 /Users/davidteren/Dropbox/03_Developement/02_Projects/1_RadioRetail/_Complete/PB_Tester/02_Music/004.mp3 /Users/davidteren/Dropbox/03_Developement/02_Projects/1_RadioRetail/_Complete/PB_Tester/03_Output/test_sp.mp3 0.06 0.06 0.3

####    Natural Numbers Logic
####
####    by David Teren
####    19-Oct-2014
####    © 2014 Radio Retail
####

####   SPECIAL NOTE:
####           I feel comfortable using MP3 files @ 160kbps as the way forward.
####            All price files, PB, Music, Adverts and such to be MP3 160 kbps
####
####           Also I am still tweaking the Audio Mastering definitions.
####           These can be updated as we go along. The important thing is that you
####           can continue to develop this process for the boxes

####   A bash script outlining the process used to prepare the audio files by:
####       1) Converting the audio files to wav
####       2) Trim the silences from the ends
####       3) Ensure perceived loudness matching
####       4) Separate the PB body into two parts
####       5) Create the silent pads that create the rhythm for flow
####       6) Concatenate the files to create the raw advert
####       7) Determine the length of the concatenated raw advert
####       8) Trim the music to the length of the concatenated raw advert and set the music volume
####       9) Define the Audio Mastering settings
####       10) Create final advert by mixing the raw advert & trimmed music & apply the mastering
####       11) Convert final advert to MP3

set -e

# Path to SOX, Lame & mp3splt
# SOX (SOX, SOXi & play) -> http://SOX.sourceforge.net
# mp3splt -> http://mp3splt.sourceforge.net/mp3splt_page/home.php
# lame - > http://lame.sourceforge.net
PATH=${PATH}:/usr/local/bin

# Set file paths for test files
MYPWD=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd $MYPWD
TESTFILES="${MYPWD}/test_files"

TEMPDIR="${MYPWD}/temp"
mkdir -p $TEMPDIR
# mp3splt="${MYPWD}/mp3splt-2.6.2/src/mp3splt"
# mp3splt="${MYPWD}/mp3splt"
# SOX="${MYPWD}/SOX-14.4.1/./sox"
# lame="${MYPWD}/lame-3.99.5/frontend/lame"
PBFILE=$1
VAL1FILE=$2
VAL2FILE=$3
MUSICFILE=$4
FINALSP=$5
GAP1=$6
GAP2=$7
GAP3=$8

PBNAME=$(basename "$1" .mp3)

# Housekeeping - Clear temp dir
# cd ${TEMPDIR} ; rm -rf *

# Split the PB file into two parts using the 4 sec silence gaps (Uses mp3splt tool)
# mp3splt -Q -s -p  th=-48,min=3.5,rm -N -d ${TEMPDIR} ${PBFILE}
sox ${PBFILE} "${TEMPDIR}/part.wav"  silence 1 0.00 -48d 1 4.0 0.1% : newfile : restart
# find "${TEMPDIR}" -size -1k -delete
# Rename the split PB parts
PBPARTA="${TEMPDIR}/pb_part001.wav"
PBPARTB="${TEMPDIR}/pb_part002.wav"

## Convert the MP3's to Wav & Trim unwanted silences from ends

sox "${TEMPDIR}/part001.wav" ${PBPARTA} silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0
sox "${TEMPDIR}/part002.wav" ${PBPARTB} silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0

# rm *.mp3 # Remove the MP3's
VAL1="${TEMPDIR}/val_1.wav"
VAL2="${TEMPDIR}/val_2.wav"
MUSIC="${TEMPDIR}/music.wav"
sox ${VAL1FILE} ${VAL1} silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0
sox ${VAL2FILE} ${VAL2} silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0
sox ${MUSICFILE} ${MUSIC} silence 1 0.01 -60d reverse silence 1 0.01 -60d reverse pad 0 0

# EBU R128 - Loudness compliance process
# The actual EBU R128 tool still needs to be determined
# mkdir temp
# for f in *.wav ; do
#      sox ${f} ./temp/${f} gain 0 #R128 process goes here - all this currently does is a zero gain change
# done
# rm -rf *.wav ;  mv ./temp/* . ; rm -rf temp

# Create/Define the pad files - NB: These are defined by studio and each voice has their own unique profile.
# We still need to determine where profile will be kept. It's very basic as there are only three variables per voice.
PADFILE1="${TEMPDIR}/pad_1.wav"
PADFILE2="${TEMPDIR}/pad_2.wav"
PADFILE3="${TEMPDIR}/pad_3.wav"
sox -n -b 16 -c 2 -r 44100 ${PADFILE1} trim 0.0 $GAP1 #Silent Pad that goes before the virst value
sox -n -b 16 -c 2 -r 44100 ${PADFILE2} trim 0.0 $GAP2 #Silent Pad that goes before the second value
sox -n -b 16 -c 2 -r 44100 ${PADFILE3} trim 0.0 $GAP3 #Silent Pad that goes before PB Part B
## Concatenate the advert
CONCATAD="${TEMPDIR}/concat_ad.wav" #The concatenated file
sox ${PBPARTA} ${PADFILE1} ${VAL1} ${PADFILE2} ${VAL2} ${PADFILE3} ${PBPARTB} ${PADFILE1} ${VAL1} ${PADFILE2} ${VAL2} -c 2 ${CONCATAD} pad 2.5 2.5
# Get the length of the concatenated file
LENG="$(soxi -D ${CONCATAD})"

# Trim the Music to match the Advert length and add in/out fades & Set the music volume
TRIMUSIC="${TEMPDIR}/music_trimmed.wav"
sox -v -1.0 ${MUSIC} ${TRIMUSIC} trim 0 ${LENG} #fade h 0:2 0 0:5

## Mastering - Description
#These settings can be tweaked more once the system goes live for optimal results. This process also offers the option to re-master other content
## Define the Audio Mastering Process
# Gain reduction to avoid clipping
 GAIN='gain -10'
# Compression - Dynamics control of the various elements that have been mixed. Glueing them together.
COMP='compand 0,0.2 -90,-90,-70,-70,-60,-30,-2,-8,-2,-7 -2'
# EQ with boost in the lows and highs with reduction in high mids to counter sibilance
EQ='equalizer 40 .71q +12 equalizer 80 1.10q +0 equalizer 240 1.80q -3 equalizer 500 .71q +0 equalizer 1000 2.90q +0 equalizer 2500 .51q +2 equalizer 8500 .71q -2.0 equalizer 17000 .71q +6'
# Filters - Hi-Pass and Lo-Pass filters
FILTER='sinc 90-12000'
# Gain - ensure not digital clipping. Especially with onboard/budget DAC's
GAIN2='gain -n -0.1'
# Set the audio processing chain
MASTERING="${GAIN} ${COMP} ${EQ} ${FILTER} ${NORM} ${GAIN2} ${RATE}"

# Mix the concatenated file and the music
MIXEDAD="${TEMPDIR}/mixed_ad.wav"
sox -m  ${CONCATAD} ${TRIMUSIC} ${MIXEDAD} ${MASTERING} fade h 0:2 0 0:3

# Convert the Final file to MP3 - Use silent mode, bitrate 160kbps, hi quality. Use lame enc -> http://lame.sourceforge.net
lame -S --silent -b 192 -h ${MIXEDAD} ${FINALSP}
# mv ${MIXEDAD} ${FINALSP}.wav

## Housekeeping - Clear out the wav files in the temp dir
 rm -rf ${TEMPDIR}/*.wav
