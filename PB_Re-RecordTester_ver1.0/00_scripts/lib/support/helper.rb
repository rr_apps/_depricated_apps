
module Helpers

  # Remove unwanted characters & words
  def array_wash(to_clean)
    to_remove = [".", "..", ".DS_Store"]
	    to_remove.each do |i|
	    	to_clean.delete_if { |x| x == i}
	    end
  end

  def vo_options(the_data)
    line = "\n" + "-" * 80 + "\n"
    puts line
 	  puts "| The Data: #{the_data}\n| The Array: #{the_array}"
    puts line
  end

  def vo_selector(voice)
    if voice.downcase.include?("eng") ; lang = "Eng" ; end
    if voice.downcase.include?("afr") ; lang = "Afr" ; end
    the_vo = voice[0..-5]
    voice = the_vo.capitalize
    return voice, lang
  end
end

include Helpers
