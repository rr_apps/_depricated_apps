require "fileutils"
require_relative './support/helper.rb'
require 'json'

## Execise
# create a snap shot of dirs, their sub dirs and contained files
# capture this to multi arrays & cleanp unwanted chars
# export the snapshot to a JSON file
# copy the files to another location using an edited version of the JSON file

time = Time.new
date_stamp = time.strftime("%Y_%m_%d-%H_%M")

profiles_path = "../../01_Profiles"
output_path   = "../../03_Output"
music_path    = "../../02_Music"
cmd           = "./support/natnumslogic.sh"

pad_1 = "0.06"
pad_2 = "0.06"
pad_3 = "0.3"

# Get the availble voices
vo_paths = []
vo_profiles = Dir.entries(profiles_path)
array_wash(vo_profiles) # => remove any unwanted stuff

## Display
puts `clear`
puts "Price & Body Re-Record Test Application".center(80)
puts "_" * 80
puts "\n\n"

## Main Process
select_no = 0
vo_profiles.each do |i|
  the_vo, lang = vo_selector(i)
  # The paths to the files
  vo_path      = File.join(profiles_path, i)
  pb_path      = File.join(vo_path, 'pb')
  vala_path    = File.join(vo_path, 'val_a')
  valb_path    = File.join(vo_path, 'val_b')

  # the files
  the_pbs    = Dir.entries(pb_path)
  the_valas = Dir.entries(vala_path)
  the_valbs = Dir.entries(valb_path)
  the_music = Dir.entries(music_path)

  # remove unwanted stuff
  array_wash(the_pbs)
  array_wash(the_valas)
  array_wash(the_valbs)
  array_wash(the_music)

  # process each fo the PB's
  the_pbs.each do |pb|

    # get the val_a random pick
    random_vala = the_valas[rand(the_valas.length)]

    # get the val_b random pick
    random_valb = the_valbs[rand(the_valbs.length)]

    # get the music random pick
    random_music = the_music[rand(the_music.length)]
    sp = "sp" + pb.sub(/^pb/, '')

     ad_pb    = File.join(pb_path, pb)
     ad_vala  = File.join(vala_path, random_vala)
     ad_valb  = File.join(valb_path, random_valb)
     ad_music = File.join(music_path, random_music)
     ad_final = File.join(output_path, i, date_stamp)
     if !Dir.exists?(ad_final) ; FileUtils.mkdir_p(ad_final) ; end
     ad_sp = File.join(ad_final, sp)

     ad_cmd = "#{File.expand_path(cmd)} #{File.expand_path(ad_pb)} #{File.expand_path(ad_vala)} #{File.expand_path(ad_valb)} #{File.expand_path(ad_music)} #{File.expand_path(ad_sp)} #{pad_1} #{pad_2} #{pad_3}"

  line =  "| Creating SP: #{sp[0..-5]}".ljust(10)
  line << " >> Using:".ljust(10)
  line << " #{the_vo}  #{lang}"
  line << "\n"
  line << "| PB: #{pb[0..-5]}".ljust(13)
  line << "| Pad 1: #{pad_1}".ljust(13)
  line << "| Val_a: #{random_vala[0..-5]}".ljust(13)
  line << "| Pad 2: #{pad_2}".ljust(13)
  line << "| Val_b: #{random_valb[0..-5]}".ljust(13)
  line << "| Pad 3: #{pad_3}".ljust(13)
  line << "\n" + "-" * 80 + "\n"

  print line
	puts `#{ad_cmd}`
  end
end
